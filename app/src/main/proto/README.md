##Установка
Устанавливаем protoc на свою тачку
На маке можно поставить так:
    
    brew install protobuf
    
Затем устанавливаем [go](https://golang.org/dl/)

Далее устанавливаем компилятор для golang:

    export GOPATH=/path/to/dir
    go get github.com/gogo/protobuf/{proto,protoc-gen-gogo,gogoproto,protoc-gen-gofast}

## Balancer 
protoc --gofast_out=plugins=grpc,Mcommon.proto=proto/pb/common:pb/balancer balancer.proto
protoc --gofast_out=pb/common common.proto


## Account
protoc --gofast_out=plugins=
grpc,
Mgogoproto/gogo.proto=lab.binatex.com/platform/account/pb/gogoproto,
Mcommon/common.proto=lab.binatex.com/platform/account/pb/common,
Mcommon/country.proto=lab.binatex.com/platform/account/pb/common,
Mcommon/currency.proto=lab.binatex.com/platform/account/pb/common,
Mcommon/paginator.proto=lab.binatex.com/platform/account/pb/common,
:../account/pb account/*.proto