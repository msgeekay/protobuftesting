#!/usr/bin/env bash

protoc --gofast_out=../pb gogoproto/*.proto
protoc --gofast_out=Mgogoproto/gogo.proto=lab.binatex.com/platform/pb/gogoproto:../pb common/*.proto
protoc --gofast_out=plugins=grpc,Mgogoproto/gogo.proto=lab.binatex.com/platform/pb/gogoproto,Mcommon/common.proto=lab.binatex.com/platform/pb/common,Mcommon/currency.proto=lab.binatex.com/platform/pb/common,Mcommon/type.proto=lab.binatex.com/platform/pb/common:../pb config/*.proto
protoc --gofast_out=plugins=grpc,Mgogoproto/gogo.proto=lab.binatex.com/platform/pb/gogoproto,Mcommon/common.proto=lab.binatex.com/platform/pb/common,Mcommon/currency.proto=lab.binatex.com/platform/pb/common,Mcommon/type.proto=lab.binatex.com/platform/pb/common,Mcommon/country.proto=lab.binatex.com/platform/pb/common,Mcommon/paginator.proto=lab.binatex.com/platform/pb/common:../pb account/*.proto
protoc --gofast_out=plugins=grpc,Mgogoproto/gogo.proto=lab.binatex.com/platform/pb/gogoproto,Mcommon/common.proto=lab.binatex.com/platform/pb/common,Mcommon/currency.proto=lab.binatex.com/platform/pb/common,Mconfig/entity.proto=lab.binatex.com/platform/pb/config,Maccount/entity.proto=lab.binatex.com/platform/pb/account:../pb trading/*.proto
protoc --gofast_out=plugins=grpc,Mgogoproto/gogo.proto=lab.binatex.com/platform/pb/gogoproto,Mcommon/common.proto=lab.binatex.com/platform/pb/common,Maccount/entity.proto=lab.binatex.com/platform/pb/account:../pb leveler/*.proto
protoc --gofast_out=plugins=grpc,Mgogoproto/gogo.proto=lab.binatex.com/platform/pb/gogoproto:../pb compressor/*.proto