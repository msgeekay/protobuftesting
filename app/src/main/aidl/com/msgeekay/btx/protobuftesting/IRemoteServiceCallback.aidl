// IRemoteServiceCallback.aidl
package com.msgeekay.btx.protobuftesting;

// Declare any non-default types here with import statements
import com.msgeekay.btx.protobuftesting.websocks.messages.response.BaseSocketResponse;

interface IRemoteServiceCallback {
    /**
     * Demonstrates some basic types that you can use as parameters
     * and return values in AIDL.
     */

    oneway void onServiceResponse(in BaseSocketResponse response);

    oneway void onServiceControlResponse(in int socketState);
}
