// IMessageBroker.aidl
package com.msgeekay.btx.protobuftesting.websocks.service;

// Declare any non-default types here with import statements
import com.msgeekay.btx.protobuftesting.websocks.messages.request.BaseSocketRequest;
import com.msgeekay.btx.protobuftesting.IRemoteServiceCallback;

interface IMessageBroker {

    /********** Socket-related requests **********/

    /**
     * Sends reuests to backend via socket connection
     */
    void sendRequest(in BaseSocketRequest request);

    /**
    * Sends reuest to start socket connection
    */
    void startSocket();

    /**
    * Sends reuest to disconnect any socket connection
    */
    void stopSocket();


    /********** Socket-related responses **********/

    /**
     * Often you want to allow a service to call back to its clients.
     * This shows how to do so, by registering a callback interface with
     * the service.
     */
    void registerCallback(IRemoteServiceCallback cb);

    /**
     * Remove a previously registered callback interface.
     */
    void unregisterCallback(IRemoteServiceCallback cb);


    /********** Service-related requests **********/

    /**
    * Sends reuest to check any possibility to stop service, and stop it if any
    */
    void checkToStop();

    /**
    * Sends reuest to stop service
    */
    void stopService();


}
