package com.msgeekay.btx.protobuftesting.websocks.service;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.util.Log;

import com.msgeekay.btx.protobuftesting.App;
import com.msgeekay.btx.protobuftesting.IRemoteServiceCallback;
import com.msgeekay.btx.protobuftesting.websocks.messages.request.BaseSocketRequest;
import com.msgeekay.btx.protobuftesting.websocks.messages.response.BaseSocketResponse;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by gkostyaev on 05/12/2017.
 */
public class ServiceCommunicationManager extends BaseAIDLServiceManager implements IServiceManager {


    private Context mContext;


    public ServiceCommunicationManager(Context context)
    {
        super(context);
        mContext = context;
    }

    protected void finalize() throws Throwable
    {
        try
        {
            unbindService();
        }
        finally
        {
            super.finalize();
        }
    }





    @Override
    public void bindService()
    {
        super.bindService();

    }

    public boolean sendMsgToService(BaseSocketRequest request)
    {
        return  super.sendMsgToService(request);
    }

    public boolean sendMsgToService(int msgID, Bundle data)
    {
        return true;
    }

    @Override
    public void stopService() {
        super.stopService();
    }


    public void unbindService()
    {
        super.unbindService();
    }



}
