package com.msgeekay.btx.protobuftesting.websocks.client;

import android.os.Handler;
import android.os.Message;

import com.msgeekay.btx.protobuftesting.utils.Logs;
import com.neovisionaries.ws.client.WebSocket;
import com.neovisionaries.ws.client.WebSocketAdapter;
import com.neovisionaries.ws.client.WebSocketException;
import com.neovisionaries.ws.client.WebSocketFrame;

import java.lang.ref.WeakReference;
import java.util.List;
import java.util.Map;

/**
 * Created by gkostyaev on 05/12/2017.
 */
public class WebSocketRouter extends WebSocketAdapter {

    private final static String TAG = WebSocketRouter.class.getSimpleName();

    private WeakReference<Handler> mMessageHandlerWR;
    private WeakReference<Handler> mStatusHandlerWR;

    public WebSocketRouter(Handler messageHandler, Handler statusHandler)
    {
        this.mMessageHandlerWR = new WeakReference<Handler>(messageHandler);
        this.mStatusHandlerWR = new WeakReference<Handler>(statusHandler);
    }

    public void onConnected(WebSocket websocket, Map<String, List<String>> headers) {
        Logs.d(TAG, "onConnected");
        if (mStatusHandlerWR != null && mStatusHandlerWR.get() != null) {
            Handler h = mStatusHandlerWR.get();
            Message m = h.obtainMessage(0, WebSocketClient.ConnectionStatus.CONNECTED);
            h.sendMessage(m);
        }
    }

    public void onConnectError(WebSocket websocket, WebSocketException cause) {
        Logs.d(TAG, "onConnectError " + cause.toString());
        if (mStatusHandlerWR != null && mStatusHandlerWR.get() != null) {
            Handler h = mStatusHandlerWR.get();
            Message m = h.obtainMessage(0, WebSocketClient.ConnectionStatus.CONNECTERROR);
            h.sendMessage(m);
        }
    }

    public void onDisconnected(WebSocket websocket,
                               WebSocketFrame serverCloseFrame,
                               WebSocketFrame clientCloseFrame,
                               boolean closedByServer) {
        Logs.d(TAG, "onDisconnected");
        if (mStatusHandlerWR != null && mStatusHandlerWR.get() != null) {
            Handler h = mStatusHandlerWR.get();
            Message m = h.obtainMessage(0, WebSocketClient.ConnectionStatus.DISCONNECTED);
            h.sendMessage(m);
        }
    }

    public void onTextMessage(WebSocket websocket, String text) {
        Logs.d(TAG, "onTextMessage");

        if (mMessageHandlerWR != null && mMessageHandlerWR.get() != null) {
            Handler h = mMessageHandlerWR.get();
            Message m = h.obtainMessage(0, text);
            h.sendMessage(m);
        }

        //onEvent(new WebSocketEvent(WebSocketEvent.MESSAGE_EVENT, text));
    }

    public void onBinaryMessage(WebSocket websocket, byte[] binary) {
        Logs.d(TAG, "onBinaryMessage");
        //onEvent(new WebSocketEvent(WebSocketEvent.BINARY_MESSAGE_EVENT, binary));

        if (mMessageHandlerWR != null && mMessageHandlerWR.get() != null) {
            Handler h = mMessageHandlerWR.get();
            Message m = h.obtainMessage(0, binary);
            h.sendMessage(m);
        }
    }

    public void onError(WebSocket websocket, WebSocketException cause) {
        Logs.d(TAG, "onError");
    }

    public void onFrameError(WebSocket websocket, WebSocketException cause, WebSocketFrame frame) {
        Logs.d(TAG, "onFrameError");
    }

    public void onMessageError(WebSocket websocket, WebSocketException cause, List<WebSocketFrame> frames) {
        Logs.d(TAG,"onMessageError");
    }

    public void onMessageDecompressionError(WebSocket websocket, WebSocketException cause, byte[] compressed) {
        Logs.d(TAG, "onMessageDecompressionError");
    }

    public void onTextMessageError(WebSocket websocket, WebSocketException cause, byte[] data) {
        Logs.d(TAG, "onTextMessageError");
    }

    public void onSendError(WebSocket websocket, WebSocketException cause, WebSocketFrame frame) {
        Logs.d(TAG, "onSendError");
    }

    public void onUnexpectedError(WebSocket websocket, WebSocketException cause) {
        Logs.d(TAG, "onUnexpectedError");
    }

    public void handleCallbackError(WebSocket websocket, Throwable cause) {
        Logs.d(TAG, "handleCallbackError");
    }

    public void onSendingHandshake(WebSocket websocket, String requestLine, List<String[]> headers) {
        Logs.d(TAG, "onSendingHandshake");
    }
}

