package com.msgeekay.btx.protobuftesting.websocks.messages.request;

import android.os.Parcel;

/**
 * Created by gkostyaev on 06/12/2017.
 */
public class BaseTradingSocketRequest extends BaseSocketRequest {

    public BaseTradingSocketRequest(Parcel in)
    {
        super(in);
    }

    public BaseTradingSocketRequest()
    {
        super();
        setType(Type.TRADING);
    }
}
