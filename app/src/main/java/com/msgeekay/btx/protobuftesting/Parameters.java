package com.msgeekay.btx.protobuftesting;

/**
 * Created by gkostyaev on 05/12/2017.
 */
public class Parameters {
    public final static String WS_URL_11 = "ws://127.0.0.1:9001";
    public final static String WS_URL_1 = "wss://staging.binatex.com:7777";
    public final static String WS_URL_2 = "wss://staging.binatex.com:7788";

    public final static boolean DEBUG = true;

}
