package com.msgeekay.btx.protobuftesting.utils;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;

import com.msgeekay.btx.protobuftesting.App;
import com.msgeekay.btx.protobuftesting.MainActivity;
import com.msgeekay.btx.protobuftesting.R;

/**
 * Created by gkostyaev on 05/12/2017.
 */
public class NotificationHelper
{
    public static int NOTIFICATION_FOREGROUND = 110;

    public static int NOTIFICATION_TRACKING = 111;
    public static int NOTIFICATION_STOP_TRACKING = 112;
    public static int NOTIFICATION_LAST_ALARM = 113;

    public static final String BINATEX_SERVICE = "com.binatex.app.websocketservice";
    public static final String BINATEX_SERVICE_TITLE = "binatex";

    public static final boolean SVC_FOREGROUND_ALLOWED_SETTING_ID = true;

    //TODO: restore notifications

    public static void checkNotification(Context context)
    {
        if (App.getInstance().isSocketing())
        {
            //do nothing
        }
        else
        {
            removeNotification(NOTIFICATION_LAST_ALARM, context);
            removeNotification(NOTIFICATION_TRACKING, context);
        }
    }

    public static void displayNotificationMessage(String message, int notifyId, boolean isCleanable,
                                                  Context context, Class activityClassForPendingIntent)
    {
        NotificationManager manager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        if (manager != null)
        {
            PendingIntent contentIntent = PendingIntent.getActivity(context, 0, new Intent(context, activityClassForPendingIntent), 0);
            NotificationCompat.Builder builder = new NotificationCompat.Builder(context);

            builder.setContentTitle(BINATEX_SERVICE_TITLE)
                    .setContentText(message)
                    .setSmallIcon(R.mipmap.ic_launcher_round)
                    .setContentIntent(contentIntent);

            if (isCleanable)
                builder.setAutoCancel(true);

            Notification notification = builder.build();
            if (!isCleanable)
                notification.flags = Notification.FLAG_NO_CLEAR;
            manager.notify(notifyId, notification);
        }
    }

    public static void removeNotification(int id, Context context)
    {
        NotificationManager manager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        if (manager != null)
        {
            manager.cancel(id);
        }
    }

    public static Notification getDefaultNotification(Context context)
    {
        Intent notificationIntent = new Intent(context.getApplicationContext(), MainActivity.class);
        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        PendingIntent contentIntent = PendingIntent.getActivity(context.getApplicationContext(), 0, notificationIntent, 0);

        return new NotificationCompat.Builder(context)
                .setContentTitle(BINATEX_SERVICE_TITLE)
                .setSmallIcon(R.mipmap.ic_launcher_round)
                .setContentIntent(contentIntent)
                .build();
    }

}
