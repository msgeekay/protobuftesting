package com.msgeekay.btx.protobuftesting.utils;

import android.util.Log;

import com.msgeekay.btx.protobuftesting.Parameters;

/**
 * Created by gkostyaev on 06/12/2017.
 */
public class Logs {

    private final static String CALLER_TAG = ":::Caller:::";

    public static void d(Object caller, String message)
    {
        String TAG = CALLER_TAG;
        try {
            TAG = caller.getClass().getSimpleName();
        } catch (Exception ex)
        {
            //do nothing
        }
        d(TAG, message);
    }

    public static void d(String TAG, String message)
    {
        if (Parameters.DEBUG)
        {
            Log.d(TAG, message);
        }
    }

    public static void e(Object caller, String message)
    {
        String TAG = CALLER_TAG;
        try {
            TAG = caller.getClass().getSimpleName();
        } catch (Exception ex)
        {
            //do nothing
        }
        e(TAG, message);
    }

    public static void e(String TAG, String message)
    {
        if (Parameters.DEBUG)
        {
            Log.e(TAG, message);
        }
    }
}
