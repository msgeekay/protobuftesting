package com.msgeekay.btx.protobuftesting.websocks.client;

import android.content.Context;
import android.os.Handler;

import com.msgeekay.btx.protobuftesting.Parameters;
import com.msgeekay.btx.protobuftesting.utils.Logs;
import com.neovisionaries.ws.client.WebSocket;
import com.neovisionaries.ws.client.WebSocketException;
import com.neovisionaries.ws.client.WebSocketExtension;
import com.neovisionaries.ws.client.WebSocketFactory;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.concurrent.CountDownLatch;

/**
 * Created by gkostyaev on 05/12/2017.
 */

public class WebSocketClient implements IWebSocketClient {

    private static final String TAG = WebSocketClient.class.getSimpleName();

    private static final int CONNECTION_TIMEOUT = 60000;   //milliseconds
    private static final int WEB_SOCKET_CONNECT_TIMEOUT = 5000;

    public enum ConnectionStatus {
        CONNECTING(0),
        CONNECTED(1),
        DISCONNECTED(2),
        CONNECTERROR(3);

        private int id;

        ConnectionStatus(int id)
        {
            this.id = id;
        }

        public static ConnectionStatus getById(int id)
        {
            ConnectionStatus retVal = DISCONNECTED;
            switch (id)
            {
                case 0:
                    retVal = CONNECTING;
                    break;
                case 1:
                    retVal = CONNECTED;
                    break;
                case 2:
                    retVal = DISCONNECTED;
                    break;
                case 3:
                    retVal = CONNECTERROR;
                    break;
            }

            return retVal;
        }
    }

    public interface SocketResponseListener {
        void onNewMessage(String message);
        void onNewMessage(byte[] bytes);
        void onStatusChange(ConnectionStatus status);
    }

    private Context appContext;
    private WebSocket mWebSocket;
    private Type mSocketType;
    private boolean connected;
    private boolean intentional;
    private CountDownLatch onConnectLatch;

    private Handler mMessageHandler;
    private Handler mStatusHandler;
    //private BinatexWebSocketListener binatexWebSocketListener;
    private WebSocketRouter webSocketRouter;
    private BTXWebSocketListener btxWebSocketListener;
    private SocketResponseListener mListener;

    //private StringPreferencesStorer urlStorer;
    private String url;

    private ConnectionStatus connectionStatus = ConnectionStatus.DISCONNECTED;

    public WebSocketClient(Context appContext, SocketResponseListener listener)
    {
        initClient(appContext, listener, Type.TRADING);
    }

    public WebSocketClient(Context appContext, SocketResponseListener listener, IWebSocketClient.Type type)
    {
        initClient(appContext, listener, type);
    }

    private void initClient(Context appContext, SocketResponseListener listener, IWebSocketClient.Type type)
    {
        this.appContext = appContext;
        this.mListener = listener;
        this.mSocketType = type;

        //urlStorer = new StringPreferencesStorer(Constants.URL_KEY);
        url = Parameters.WS_URL_1;

        //mClient = createWebSocketClient();


        mMessageHandler = new Handler(msg ->
        {
            if (msg != null && msg.obj != null)
            {
                if (msg.obj instanceof String && mListener != null)
                    mListener.onNewMessage((String) msg.obj);
                if (msg.obj instanceof byte[] && mListener != null)
                    mListener.onNewMessage((byte[] ) msg.obj);
            }
            return true;
        });
        mStatusHandler = new Handler(msg ->
        {
            ConnectionStatus cs = (ConnectionStatus) msg.obj;
            if (mListener != null)
                mListener.onStatusChange(cs);
            onNewConnectionStatus(cs);
            return true;
        });
        webSocketRouter = new WebSocketRouter(mMessageHandler, mStatusHandler);
        btxWebSocketListener = new BTXWebSocketListener(mMessageHandler, mStatusHandler);
    }



    private void onNewConnectionStatus(ConnectionStatus newStatus)
    {
        switch (newStatus)
        {
            case CONNECTED:
                //onEvent(new WebSocketEvent(WebSocketEvent.OPEN_EVENT));
                //EventBus.getDefault().post(new WebSocketIsOpenEvent());
                connected = true;
                Logs.d(this, "Socket is opened. WS State=" + mWebSocket.getState().name());
                break;
            case CONNECTERROR:
                mWebSocket.disconnect();
                connected = false;
                intentional = false;
//        EventBus.getDefault().post(new DisconnectEvent(false));
                break;
            case DISCONNECTED:
                //onEvent(new WebSocketEvent(WebSocketEvent.CLOSE_EVENT));
                connected = false;
                //EventBus.getDefault().post(new DisconnectEvent(intentional));
                intentional = false;
                mWebSocket = null;
                break;
        }
        this.connectionStatus = newStatus;

//        if (onConnectLatch != null && onConnectLatch.getCount() > 0)
//            onConnectLatch.countDown();

    }

    public boolean isConnected() {
        return (mWebSocket != null) && (mWebSocket.isOpen());
    }

    public boolean isConnecting() {
        return connectionStatus == ConnectionStatus.CONNECTING;
    }

    public void disconnectWebSocket() {
        disconnect();
        intentional = true;
        connected = false;

    }

    public void connectToWebSocket() {
        connect();
    }

    private synchronized void connect()
    {
        //url = urlStorer.get(appContext);


        if (url == null || isConnected() || isConnecting()) {
            return;
        }

        try
        {
            connectionStatus = ConnectionStatus.CONNECTING;
            //onConnectLatch = new CountDownLatch(1);
            URI serverURI = new URI(url);
            mWebSocket = new WebSocketFactory()
                    .setConnectionTimeout(WEB_SOCKET_CONNECT_TIMEOUT)
                    .createSocket(serverURI)
                    //.addListener(webSocketRouter)
                    .addListener(btxWebSocketListener)
                    //.addExtension(WebSocketExtension.PERMESSAGE_DEFLATE)
                    .connectAsynchronously();
//            try
//            {
//                if (!onConnectLatch.await(1, TimeUnit.MINUTES)) {
//                    Logs.e(this, "can not connect within 1 minutes");
//                    //warning("recordRoute can not finish within 1 minutes");
//                }
//            }
//            catch (Exception ex)
//            {
//                Logs.e(this, ex.getMessage());
//            }
        } catch (IOException e) {
            e.printStackTrace();
            connected = false;
            connectionStatus = ConnectionStatus.DISCONNECTED;
            //onConnectLatch.countDown();
        } catch (URISyntaxException e) {
            e.printStackTrace();
            connected = false;
            connectionStatus = ConnectionStatus.DISCONNECTED;
            //onConnectLatch.countDown();
        }



    }

    private synchronized void disconnect() {
        if (mWebSocket != null)
            mWebSocket.disconnect();
        //mListener = null;
        mMessageHandler.removeCallbacksAndMessages(null);
        mStatusHandler.removeCallbacksAndMessages(null);
        connectionStatus = ConnectionStatus.DISCONNECTED;
    }

    public synchronized void sendMessage(String message) {
        mWebSocket.sendText(message);
    }

    public synchronized void sendMessage(byte[] bytes) {
        if (isConnected()) {
            mWebSocket.sendBinary(bytes);
            Logs.d(this, "Socket is opened. WS State=" + mWebSocket.getState().name());
            Logs.d(this, "Socket is opened. message was sent. Message=" + bytes);
        }
        else
        {
            Logs.d(this, "Socket isn't opened. message wasn't sent");
            //TODO: add messages to the queue
        }
    }

    @Override
    public Type getType() {
        return mSocketType;
    }

//    @Override
//    public Object getApi() {
//        return null;
//    }
//
//    @Override
//    public Object getRestApi() {
//        return null;
//    }
//
//    @Override
//    public BaseResponse parseErrorBody(Object objResponse) {
//        return null;
//    }

}

