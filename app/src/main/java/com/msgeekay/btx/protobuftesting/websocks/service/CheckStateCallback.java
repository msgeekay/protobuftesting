package com.msgeekay.btx.protobuftesting.websocks.service;

/**
 * Created by gkostyaev on 05/12/2017.
 */

public interface CheckStateCallback {
    public void onCheckStateCallback();
}
