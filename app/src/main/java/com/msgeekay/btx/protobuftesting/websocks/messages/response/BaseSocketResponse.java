package com.msgeekay.btx.protobuftesting.websocks.messages.response;

import android.os.Parcel;
import android.os.Parcelable;

import com.msgeekay.btx.protobuftesting.websocks.IConnectionType;
import com.msgeekay.btx.protobuftesting.websocks.messages.request.BaseSocketRequest;

/**
 * Created by gkostyaev on 06/12/2017.
 */
public class BaseSocketResponse implements Parcelable {

    private String responseTypeName;
    private byte[] responseBytes;

    public BaseSocketResponse()
    {}

    public BaseSocketResponse(String responseTypeName, byte[] responseBytes)
    {
        this.responseTypeName = responseTypeName;
        this.responseBytes = responseBytes;
    }

    public BaseSocketResponse(Parcel in)
    {
        this.responseTypeName = in.readString();
        responseBytes = new byte[in.readInt()];
        in.readByteArray(responseBytes);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public byte[] getResponseAsBytes()
    {
        //throw new Exception("Not overriden method");
        return responseBytes;
    }

    @Override
    public void writeToParcel(Parcel out, int flags) {
        out.writeString(responseTypeName);
        byte[] arr =  getResponseAsBytes();
        out.writeInt(arr.length);
        out.writeByteArray(arr);
    }

    public static final Parcelable.Creator<BaseSocketResponse> CREATOR
            = new Parcelable.Creator<BaseSocketResponse>() {
        public BaseSocketResponse createFromParcel(Parcel in) {
            return new BaseSocketResponse(in);
        }

        public BaseSocketResponse[] newArray(int size) {
            return new BaseSocketResponse[size];
        }
    };
}
