package com.msgeekay.btx.protobuftesting.websocks.service;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.RemoteException;
import android.util.Log;

import com.msgeekay.btx.protobuftesting.App;
import com.msgeekay.btx.protobuftesting.IRemoteServiceCallback;
import com.msgeekay.btx.protobuftesting.utils.Logs;
import com.msgeekay.btx.protobuftesting.websocks.client.WebSocketClient;
import com.msgeekay.btx.protobuftesting.websocks.messages.request.BaseSocketRequest;
import com.msgeekay.btx.protobuftesting.websocks.messages.response.BaseSocketResponse;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by gkostyaev on 06/12/2017.
 */
public abstract class BaseAIDLServiceManager {

    public final static String SERVICE_AIDL_INTENT_ACTION = "SERVICE_AIDL_INTENT_ACTION";

    private Context mContext;
    private IMessageBroker mRemoteService = null;
    private boolean isBoundToService = false;

    private List<BaseSocketRequest> msgQueue = new ArrayList<BaseSocketRequest>();

    public BaseAIDLServiceManager(Context context)
    {
        this.mContext = context;
    }

    /**
     * Defines callbacks for service binding, passed to bindService()
     */
    private ServiceConnection mConnectionAidl = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {

            synchronized (this) {
                mRemoteService = IMessageBroker.Stub.asInterface(service);

                // We want to monitor the service for as long as we are
                // connected to it.
                try {
                    mRemoteService.registerCallback(mCallback);
                } catch (RemoteException e) {
                    // In this case the service has crashed before we could even
                    // do anything with it; we can count on soon being
                    // disconnected (and then reconnected if it can be restarted)
                    // so there is no need to do anything here.
                }

                isBoundToService = true;

                startSocket();

                List<BaseSocketRequest> toRemove = new ArrayList<BaseSocketRequest>();
                for (BaseSocketRequest msq : msgQueue)
                {
                    if (sendMsgToService(msq))
                        toRemove.add(msq);
                }

                msgQueue.removeAll(toRemove);
            }
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {

            Logs.d(getClass().getName(), "onServiceDisconnected");
            try {
                unbindService();
            } catch (Exception ex)
            {
                Logs.e(this, ex.getMessage());
            }
            mRemoteService = null;
            isBoundToService = false;
        }
    };


    public void bindService()
    {
        Intent intent = new Intent(mContext, WebSocketService.class);
        intent.setAction(SERVICE_AIDL_INTENT_ACTION);
        mContext.bindService(intent, mConnectionAidl, Context.BIND_AUTO_CREATE);
    }

    public void unbindService()
    {
        if (isBoundToService)
        {
            mContext.unbindService(mConnectionAidl);
            isBoundToService = false;
            Log.d(getClass().getName(), "unbindService");
        }
    }

    public boolean sendMsgToService(BaseSocketRequest request)
    {
        synchronized (this)
        {
            if (!isBoundToService || mRemoteService == null)
            {
                msgQueue.add(request);
                Log.d(getClass().getName(), "fail sendMsgToService. add to queue " + String.valueOf(request.getType()));
                return false;
            }

            try
            {
                mRemoteService.sendRequest(request);
                Log.d(getClass().getName(), "sendMsgToService " + String.valueOf(request.getType()));
                return true;
            }
            catch (RemoteException e)
            {
                Log.d(getClass().getName(), e.toString());
                //CrashHelper.reportCrash(e);
            }

            return false;
        }
    }


    public void startSocket()
    {
        //sendMsgToService(WebSocketService.MSG_START_SOCKET, null);
        if (mRemoteService != null) {
            try {
                mRemoteService.startSocket();
            } catch (Exception ex)
            {
                String s = ex.getMessage();
                if (s == null)
                    s = "WTF happened on startSocket of remoteService";
                Logs.e(this, s);
            }
        }

        App.getInstance().setSocketing(true);
        //Preferences.StaticAccess.setBoolean(Preferences.IS_TRACKING, mContext, true);
    }

    public void stopSocket()
    {
        //sendMsgToService(WebSocketService.MSG_STOP_SOCKET, null);
        if (mRemoteService != null) {
            try {
                mRemoteService.stopSocket();
            } catch (Exception ex)
            {
                Logs.e(this, ex.getMessage());
            }
        }
        //Preferences.StaticAccess.setBoolean(Preferences.IS_TRACKING, mContext, false);
        App.getInstance().setSocketing(false);
    }

    public void checkToStop()
    {
        //sendMsgToService(WebSocketService.MSG_CHECK_TO_STOP, null);
        if (mRemoteService != null) {
            try {
                mRemoteService.checkToStop();
            } catch (Exception ex)
            {
                Logs.e(this, ex.getMessage());
            }
        }
    }

    public void stopService()
    {
        //sendMsgToService(WebSocketService.MSG_STOP_SERVICE, null);
        if (mRemoteService != null) {
            try {
                mRemoteService.stopService();
            } catch (Exception ex)
            {
                Logs.e(this, ex.getMessage());
            }
        }
        unbindService();
    }





    /**
     * This implementation is used to receive callbacks from the remote
     * service.
     */
    private IRemoteServiceCallback mCallback = new IRemoteServiceCallback.Stub() {

        /**
         * This is called by the remote service regularly to tell us about
         * new Responses.  Note that IPC calls are dispatched through a thread
         * pool running in each process, so the code executing here will
         * NOT be running in our main thread like most other things -- so,
         * to update the UI, we need to use a Handler to hop over there.
         */
        @Override
        public void onServiceResponse(BaseSocketResponse response) throws RemoteException {
            //mHandler.sendMessage(mHandler.obtainMessage(BUMP_MSG, response, 0));
            mHandler.sendMessage(mHandler.obtainMessage(MSG_SOCK_RESPONSE, 777, 0));
        }

        @Override
        public void onServiceControlResponse(int socketState) throws RemoteException {
            //mHandler.sendMessage(mHandler.obtainMessage(BUMP_MSG, response, 0));
            mHandler.sendMessage(mHandler.obtainMessage(MSG_SOCK_STATE, socketState, 0));
        }

    };

    private static final int MSG_SOCK_RESPONSE = 1;
    private static final int MSG_SOCK_STATE = 2;

    private Handler mHandler = new Handler() {
        @Override public void handleMessage(Message msg) {
            switch (msg.what) {
                case MSG_SOCK_RESPONSE:
                    //mCallbackText.setText("Received from service: " + msg.arg1);
                    Logs.d(BaseAIDLServiceManager.this, "Received socket response: ");
                    break;
                case MSG_SOCK_STATE:
                    WebSocketClient.ConnectionStatus status = WebSocketClient.ConnectionStatus.getById(msg.arg1);
                    Logs.d(BaseAIDLServiceManager.this, "Received socket status: " + status.name());
                    //mCallbackText.setText("Received from service: " + msg.arg1);
                    break;
                default:
                    super.handleMessage(msg);
            }
        }

    };
}
