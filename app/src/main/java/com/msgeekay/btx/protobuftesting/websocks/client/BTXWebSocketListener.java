package com.msgeekay.btx.protobuftesting.websocks.client;

import android.os.Handler;
import android.os.Message;

import com.msgeekay.btx.protobuftesting.utils.Logs;
import com.neovisionaries.ws.client.WebSocket;
import com.neovisionaries.ws.client.WebSocketException;
import com.neovisionaries.ws.client.WebSocketFrame;
import com.neovisionaries.ws.client.WebSocketListener;
import com.neovisionaries.ws.client.WebSocketState;

import java.lang.ref.WeakReference;
import java.util.List;
import java.util.Map;

/**
 * Created by grigoriykatz on 12/7/17.
 */

public class BTXWebSocketListener implements WebSocketListener {

    private final static String TAG = BTXWebSocketListener.class.getSimpleName();

    private WeakReference<Handler> mMessageHandlerWR;
    private WeakReference<Handler> mStatusHandlerWR;

    public BTXWebSocketListener(Handler messageHandler, Handler statusHandler)
    {
        this.mMessageHandlerWR = new WeakReference<Handler>(messageHandler);
        this.mStatusHandlerWR = new WeakReference<Handler>(statusHandler);
    }


    @Override
    public void onStateChanged(WebSocket websocket, WebSocketState newState) throws Exception {
        Logs.d(TAG, "onStateChanged: " + newState);
    }

    @Override
    public void onConnected(WebSocket websocket, Map<String, List<String>> headers) throws Exception {
        Logs.d(TAG, "onConnected");
        if (mStatusHandlerWR != null && mStatusHandlerWR.get() != null) {
            Handler h = mStatusHandlerWR.get();
            Message m = h.obtainMessage(0, WebSocketClient.ConnectionStatus.CONNECTED);
            h.sendMessage(m);
        }
    }

    @Override
    public void onConnectError(WebSocket websocket, WebSocketException cause) throws Exception {
        Logs.d(TAG, "onConnectError " + cause.toString());
        if (mStatusHandlerWR != null && mStatusHandlerWR.get() != null) {
            Handler h = mStatusHandlerWR.get();
            Message m = h.obtainMessage(0, WebSocketClient.ConnectionStatus.CONNECTERROR);
            h.sendMessage(m);
        }
    }

    @Override
    public void onDisconnected(WebSocket websocket, WebSocketFrame serverCloseFrame, WebSocketFrame clientCloseFrame, boolean closedByServer) throws Exception {
        Logs.d(TAG, "onDisconnected");
        if (mStatusHandlerWR != null && mStatusHandlerWR.get() != null) {
            Handler h = mStatusHandlerWR.get();
            Message m = h.obtainMessage(0, WebSocketClient.ConnectionStatus.DISCONNECTED);
            h.sendMessage(m);
        }
    }

    @Override
    public void onFrame(WebSocket websocket, WebSocketFrame frame) throws Exception {
        Logs.d(TAG, "onFrame");
    }

    @Override
    public void onContinuationFrame(WebSocket websocket, WebSocketFrame frame) throws Exception {
        Logs.d(TAG, "onContinuationFrame");
    }

    @Override
    public void onTextFrame(WebSocket websocket, WebSocketFrame frame) throws Exception {
        Logs.d(TAG, "onTextFrame");
    }

    @Override
    public void onBinaryFrame(WebSocket websocket, WebSocketFrame frame) throws Exception {
        Logs.d(TAG, "onBinaryFrame");
    }

    @Override
    public void onCloseFrame(WebSocket websocket, WebSocketFrame frame) throws Exception {
        Logs.d(TAG, "onCloseFrame");
    }

    @Override
    public void onPingFrame(WebSocket websocket, WebSocketFrame frame) throws Exception {
        Logs.d(TAG, "onPingFrame");
    }

    @Override
    public void onPongFrame(WebSocket websocket, WebSocketFrame frame) throws Exception {
        Logs.d(TAG, "onPongFrame");
    }

    @Override
    public void onTextMessage(WebSocket websocket, String text) throws Exception {
        Logs.d(TAG, "onTextMessage");

        if (mMessageHandlerWR != null && mMessageHandlerWR.get() != null) {
            Handler h = mMessageHandlerWR.get();
            Message m = h.obtainMessage(0, text);
            h.sendMessage(m);
        }
    }

    @Override
    public void onBinaryMessage(WebSocket websocket, byte[] binary) throws Exception {
        Logs.d(TAG, "onBinaryMessage");
        //onEvent(new WebSocketEvent(WebSocketEvent.BINARY_MESSAGE_EVENT, binary));

        if (mMessageHandlerWR != null && mMessageHandlerWR.get() != null) {
            Handler h = mMessageHandlerWR.get();
            Message m = h.obtainMessage(0, binary);
            h.sendMessage(m);
        }
    }

    @Override
    public void onSendingFrame(WebSocket websocket, WebSocketFrame frame) throws Exception {
        Logs.d(TAG, "onSendingFrame");
    }

    @Override
    public void onFrameSent(WebSocket websocket, WebSocketFrame frame) throws Exception {
        Logs.d(TAG, "onFrameSent");
    }

    @Override
    public void onFrameUnsent(WebSocket websocket, WebSocketFrame frame) throws Exception {
        Logs.d(TAG, "onFrameUnsent");
    }

    @Override
    public void onError(WebSocket websocket, WebSocketException cause) throws Exception {
        Logs.d(TAG, "onError");
    }

    @Override
    public void onFrameError(WebSocket websocket, WebSocketException cause, WebSocketFrame frame) throws Exception {
        Logs.d(TAG, "onFrameError");
    }

    @Override
    public void onMessageError(WebSocket websocket, WebSocketException cause, List<WebSocketFrame> frames) throws Exception {
        Logs.d(TAG,"onMessageError");
    }

    @Override
    public void onMessageDecompressionError(WebSocket websocket, WebSocketException cause, byte[] compressed) throws Exception {
        Logs.d(TAG, "onMessageDecompressionError");

    }

    @Override
    public void onTextMessageError(WebSocket websocket, WebSocketException cause, byte[] data) throws Exception {
        Logs.d(TAG, "onTextMessageError");

    }

    @Override
    public void onSendError(WebSocket websocket, WebSocketException cause, WebSocketFrame frame) throws Exception {
        Logs.d(TAG, "onSendError");
    }

    @Override
    public void onUnexpectedError(WebSocket websocket, WebSocketException cause) throws Exception {
        Logs.d(TAG, "onUnexpectedError");
    }

    @Override
    public void handleCallbackError(WebSocket websocket, Throwable cause) throws Exception {
            Logs.d(TAG, "handleCallbackError");
    }

    @Override
    public void onSendingHandshake(WebSocket websocket, String requestLine, List<String[]> headers) throws Exception {
        Logs.d(TAG, "onSendingHandshake");
    }
}
