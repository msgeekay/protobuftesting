package com.msgeekay.btx.protobuftesting;

import android.content.Context;
import android.content.Intent;
import android.support.multidex.MultiDex;
import android.support.multidex.MultiDexApplication;

import com.msgeekay.btx.protobuftesting.utils.ProcessUtils;
import com.msgeekay.btx.protobuftesting.websocks.service.IServiceManager;
import com.msgeekay.btx.protobuftesting.websocks.service.ServiceCommunicationManager;
import com.msgeekay.btx.protobuftesting.websocks.service.WebSocketService;

/**
 * Created by gkostyaev on 05/12/2017.
 */
public class App extends MultiDexApplication {

    private static App sInstance;
    private IServiceManager scm;

    private boolean isSocketing = false;

    @Override
    public void onCreate() {
        super.onCreate();
        sInstance = this;

        if (ProcessUtils.amIinMainProcess())
            init();
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    private void init()
    {
        Intent intent = new Intent(getApplicationContext(), WebSocketService.class);
        startService(intent);

        this.scm = new ServiceCommunicationManager(this);
        this.scm.bindService();
        this.scm.startSocket();
    }

    public IServiceManager getScm()
    {
        return this.scm;
    }

    public static App getInstance() {
        return sInstance;
    }

    public boolean isSocketing() {
        return isSocketing;
    }

    public void setSocketing(boolean socketing) {
        isSocketing = socketing;
    }
}
