package com.msgeekay.btx.protobuftesting;

import android.os.Bundle;
import android.os.Handler;
import android.os.Parcel;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;

import com.msgeekay.btx.protobuftesting.utils.Logs;
import com.msgeekay.btx.protobuftesting.websocks.IConnectionType;
import com.msgeekay.btx.protobuftesting.websocks.messages.request.BaseSocketRequest;
import com.msgeekay.btx.protobuftesting.websocks.messages.request.HelloSocketRequest;
import com.msgeekay.btx.protobuftesting.websocks.service.IServiceManager;
import com.msgeekay.btx.protobuftesting.websocks.service.ServiceCommunicationManager;

import balancer.BalancerOuterClass;
import common.Type;
import trading.Entity;


public class MainActivity extends AppCompatActivity {

    public static final String TAG = MainActivity.class.getName();
    private IServiceManager scm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Hello was sent", Snackbar.LENGTH_LONG)
                        .setAction("Action Hello", null).show();
                if (scm != null)
                {
                    //protoTesting2(1);
                    //scm.sendHello();
                    protoTestingSubs();
                }
            }
        });

        if (scm == null)
            scm = App.getInstance().getScm();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    Handler h = new Handler();
    int i = 0;
    private void protoTesting(int di)
    {
        i += di;
        Logs.d(this, "protoTestings for i=" + i);
        HelloSocketRequest req = new HelloSocketRequest(i, i);
        scm.sendMsgToService(req);
        if (i < 40)
            h.postDelayed(new Runnable() {
                @Override
                public void run() {
                    protoTesting(1);
                }
            }, 1000);
        else
            i = 0;
    }

    int j = 1;
    int k = 0;

    private void protoTesting2(int di)
    {
        k += di;
        if (k < 40)
        {
            Logs.d(this, "protoTestings for j=" + j + " and k=" + k);
            HelloSocketRequest req = new HelloSocketRequest(j, k);
            scm.sendMsgToService(req);

            h.postDelayed(new Runnable() {
                @Override
                public void run() {
                    protoTesting2(1);
                }
            }, 1500);
        }
        else
        {
            j++;
            k = 0;
            if (j < 40)
                h.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        protoTesting2(1);
                    }
                }, 1500);
            else {
                j=0;k=0;
                return;
            }
        }
    }

    private void protoTesting3()
    {
        Type.Int64 id = Type.Int64.newBuilder().setValue(0).build();
        Logs.d(this, "protoTesting2 for usr=" + id.getValue());
        HelloSocketRequest req = new HelloSocketRequest(id);
        scm.sendMsgToService(req);

    }

    private void protoTestingSubs()
    {
        //protoTesting3();

        Entity.Subscription subs = Entity.Subscription.newBuilder().setTool("BTC/USD").build();
        Entity.Event event = Entity.Event.newBuilder().setType(Entity.EventType.EventTypeSubscription)
                .setData(subs.toByteString()).build();

        Logs.e(this, "Message2 as byte[] == " + event.toByteArray());
        Logs.e(this, "Message2 as ByteString == " + event.toByteString());
        Logs.e(this, "Message2 as getData=(ByteString) == " + event.getData());

        Parcel out = Parcel.obtain();
        out.writeString(IConnectionType.Type.TRADING.name());
        byte[] arr =  event.toByteArray();
        out.writeInt(arr.length);
        out.writeByteArray(arr);
        BaseSocketRequest req = new BaseSocketRequest(out);
        scm.sendMsgToService(req);
        out.recycle();

    }
}
