package com.msgeekay.btx.protobuftesting.websocks.messages.response;

import com.google.protobuf.ByteString;
import com.msgeekay.btx.protobuftesting.utils.Logs;

/**
 * Created by gkostyaev on 06/12/2017.
 */
public class TradeConfigSocketResponse extends BaseSocketResponse {

    private trading.Entity.TradeConfig tradeConfig;

    public TradeConfigSocketResponse(trading.Entity.TradeConfig config)
    {
        this.tradeConfig = config;
    }

    public TradeConfigSocketResponse(ByteString byteString)
    {
        try {
            this.tradeConfig = trading.Entity.TradeConfig.parseFrom(byteString);
        } catch (Exception ex)
        {
            Logs.e(this, ex.getMessage());
        }
    }

}
