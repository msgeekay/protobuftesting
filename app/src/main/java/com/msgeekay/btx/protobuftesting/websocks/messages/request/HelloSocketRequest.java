package com.msgeekay.btx.protobuftesting.websocks.messages.request;

import android.os.Parcel;

import com.msgeekay.btx.protobuftesting.utils.Logs;

import common.Type;
import trading.Entity;

/**
 * Created by gkostyaev on 06/12/2017.
 */
public class HelloSocketRequest extends BaseTradingSocketRequest {

    private long userId;
    private long accountId;
    private common.Type.Int64 id;

    public HelloSocketRequest(Parcel in) {
        super(in);
    }

    public HelloSocketRequest(long userId, long accountId)
    {
        this.userId = userId;
        this.accountId = accountId;
        setType(Type.TRADING);
    }

    public HelloSocketRequest(common.Type.Int64 id)
    {
        this.id = id;
    }

    @Override
    public byte[] getRequestAsBytes() {
        trading.Entity.Hello hello = Entity.Hello.newBuilder().setUser(id.getValue()).setAccount(id.getValue()).build();
        trading.Entity.Event event = Entity.Event.newBuilder().setType(Entity.EventType.EventTypeHello)
                .setData(hello.toByteString()).build();

        Logs.e(this, "Message as byte[] == " + event.toByteArray());
        Logs.e(this, "Message as ByteString == " + event.toByteString());
        Logs.e(this, "Message as getData=(ByteString) == " + event.getData());

        return event.toByteArray();
    }
}
