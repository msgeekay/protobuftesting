package com.msgeekay.btx.protobuftesting.utils;

import com.msgeekay.btx.protobuftesting.App;

import android.os.Process;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by gkostyaev on 06/12/2017.
 */
public class ProcessUtils {

    private final static String TAG = ProcessUtils.class.getSimpleName();

    private static final Object SYNC_amIinMainProcess = new Object();
    private static Boolean mCached_amIinMainProcess = null;

    public static boolean amIinMainProcess() {
        boolean booleanValue;
        synchronized (SYNC_amIinMainProcess) {
            if (mCached_amIinMainProcess != null) {
                booleanValue = mCached_amIinMainProcess.booleanValue();
            } else {
                mCached_amIinMainProcess = Boolean.valueOf(App.getInstance().getPackageName().equals(getProcessName()));
                booleanValue = mCached_amIinMainProcess.booleanValue();
            }
        }
        return booleanValue;
    }

    private static String getProcessName() {
        Throwable th;
        BufferedReader bufferedReader = null;
        try {
            BufferedReader cmdlineReader = new BufferedReader(new InputStreamReader(new FileInputStream("/proc/" + Process.myPid() + "/cmdline"), "iso-8859-1"));
            try {
                StringBuilder processName = new StringBuilder();
                while (true) {
                    int c = cmdlineReader.read();
                    if (c <= 0) {
                        break;
                    }
                    processName.append((char) c);
                }
                Logs.d(TAG, "Retrieved process name is '" + processName.toString() + "'");
                String stringBuilder = processName.toString();
                if (cmdlineReader != null) {
                    try {
                        cmdlineReader.close();
                    } catch (IOException e) {
                    }
                }
                return stringBuilder;
            } catch (IOException e2) {
                bufferedReader = cmdlineReader;
                try {
                    throw new RuntimeException("Unable to retrieve the process name");
                } catch (Throwable th2) {
                    th = th2;
                    if (bufferedReader != null) {
                        try {
                            bufferedReader.close();
                        } catch (IOException e3) {
                        }
                    }
                    throw new Exception(th);
                }
            } catch (Throwable th3) {
                th = th3;
                bufferedReader = cmdlineReader;
                if (bufferedReader != null) {
                    bufferedReader.close();
                }
                throw new Exception(th);
            }
        } catch (Exception e4) {
            throw new RuntimeException("Unable to retrieve the process name");
        }
    }
}
