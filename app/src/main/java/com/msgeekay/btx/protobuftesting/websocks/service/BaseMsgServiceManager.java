package com.msgeekay.btx.protobuftesting.websocks.service;

import android.app.Service;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.support.annotation.Nullable;
import android.util.Log;

import com.msgeekay.btx.protobuftesting.App;
import com.msgeekay.btx.protobuftesting.utils.Logs;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by gkostyaev on 06/12/2017.
 */
public abstract class BaseMsgServiceManager {

    public final static String SERVICE_MSG_INTENT_ACTION = "SERVICE_MSG_INTENT_ACTION";

    private Messenger mControlService = null;

    private boolean isBoundToService = false;
    private Context mContext;
    private List<Message> msgQueue = new ArrayList<Message>();

    public BaseMsgServiceManager(Context appContext)
    {
        this.mContext = appContext;
    }

    /**
     * Defines callbacks for service binding, passed to bindService()
     */
    private ServiceConnection mConnection = new ServiceConnection()
    {

        @Override
        public void onServiceConnected(ComponentName className, IBinder service)
        {
            synchronized (this)
            {
                Log.d(getClass().getName(), "onServiceConnected");
                mControlService = new Messenger(service);
                isBoundToService = true;

                startSocket();

                List<Message> toRemove = new ArrayList<Message>();
                for (Message msq : msgQueue)
                {
                    if (sendMsgToService(msq.what, msq.getData()))
                        toRemove.add(msq);
                }

                msgQueue.removeAll(toRemove);
            }
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0)
        {
            //Timber.w(getClass().getName(), "onServiceDisconnected");
            Logs.d(getClass().getName(), "onServiceDisconnected");
            try {
                unbindService();
            } catch (Exception ex)
            {
                Logs.e(this, ex.getMessage());
            }
            mControlService = null;
            isBoundToService = false;
        }
    };

    public void bindService()
    {
        Intent intent = new Intent(mContext, WebSocketService.class);
        intent.setAction(SERVICE_MSG_INTENT_ACTION);
        mContext.bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
    }

    public boolean sendMsgToService(int msgID, Bundle data)
    {
        synchronized (this)
        {
            // Create and send a message to the service, using a supported 'what' value
            Message msg = Message.obtain(null, msgID, 0, 0);
            if (data != null)
                msg.setData(data);

            if (!isBoundToService || mControlService == null)
            {
                msgQueue.add(msg);
                Log.d(getClass().getName(), "fail sendMsgToService. add to queue " + String.valueOf(msg.what));
                return false;
            }

            try
            {
                mControlService.send(msg);
                Log.d(getClass().getName(), "sendMsgToService " + String.valueOf(msg.what));
                return true;
            }
            catch (RemoteException e)
            {
                Log.d(getClass().getName(), e.toString());
                //CrashHelper.reportCrash(e);
            }

            return false;
        }
    }

    public void startSocket()
    {
        sendMsgToService(WebSocketService.MSG_START_SOCKET, null);
        App.getInstance().setSocketing(true);
        //Preferences.StaticAccess.setBoolean(Preferences.IS_TRACKING, mContext, true);
    }

    public void sendHello()
    {
        sendMsgToService(WebSocketService.MSG_DT_HELLO, null);
    }

    public void stopSocket()
    {
        sendMsgToService(WebSocketService.MSG_STOP_SOCKET, null);
        //Preferences.StaticAccess.setBoolean(Preferences.IS_TRACKING, mContext, false);
        App.getInstance().setSocketing(false);
    }

    public void checkToStop()
    {
        sendMsgToService(WebSocketService.MSG_CHECK_TO_STOP, null);
    }

    private void stopService()
    {
        sendMsgToService(WebSocketService.MSG_STOP_SERVICE, null);
        unbindService();
    }

    public void unbindService()
    {
        if (isBoundToService)
        {
            mContext.unbindService(mConnection);
            isBoundToService = false;
            Log.d(getClass().getName(), "unbindService");
        }
    }
}
