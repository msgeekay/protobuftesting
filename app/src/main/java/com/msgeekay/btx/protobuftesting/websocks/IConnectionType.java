package com.msgeekay.btx.protobuftesting.websocks;

/**
 * Created by gkostyaev on 06/12/2017.
 */
public interface IConnectionType {
    enum Type {
        TRADING(0),
        OTHER(1);

        private int id;

        Type(int id)
        {
            this.id = id;
        }

        public static Type getById(int id)
        {
            Type retVal = OTHER;

            switch (id)
            {
                case 0:
                    retVal = TRADING;
                    break;
                case 1:
                    retVal = OTHER;
                    break;
            }

            return retVal;
        }

        public int getId()
        {
            return this.id;
        }
    }

    Type getType();
}
