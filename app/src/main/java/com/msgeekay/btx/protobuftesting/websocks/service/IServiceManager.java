package com.msgeekay.btx.protobuftesting.websocks.service;

import android.os.Bundle;

import com.msgeekay.btx.protobuftesting.websocks.messages.request.BaseSocketRequest;

/**
 * Created by grigoriykatz on 12/7/17.
 */

public interface IServiceManager {

    void bindService();
    void unbindService();
    boolean sendMsgToService(BaseSocketRequest request);
    boolean sendMsgToService(int msgID, Bundle data);

    void startSocket();
    void stopSocket();

    void checkToStop();
    void stopService();


}
