package com.msgeekay.btx.protobuftesting.websocks.messages.request;

import android.os.Parcel;
import android.os.Parcelable;

import com.msgeekay.btx.protobuftesting.websocks.IConnectionType;

/**
 * Created by gkostyaev on 06/12/2017.
 */
public class BaseSocketRequest implements IConnectionType, Parcelable {
    private Type connectionType;
    private byte[] requestBytes;

    @Override
    public Type getType() {
        return connectionType;
    }

    protected void setType(Type type)
    {
        this.connectionType = type;
    }

    public BaseSocketRequest()
    {}

    public BaseSocketRequest(Parcel in)
    {
        connectionType = Type.getById(in.readInt());
        requestBytes = new byte[in.readInt()];
        in.readByteArray(requestBytes);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public byte[] getRequestAsBytes()
    {
        //throw new Exception("Not overriden method");
        return requestBytes;
    }

    @Override
    public void writeToParcel(Parcel out, int flags) {
        out.writeInt(connectionType.getId());
        byte[] arr =  getRequestAsBytes();
        out.writeInt(arr.length);
        out.writeByteArray(arr);
    }

    public static final Parcelable.Creator<BaseSocketRequest> CREATOR
            = new Parcelable.Creator<BaseSocketRequest>() {
        public BaseSocketRequest createFromParcel(Parcel in) {
            return new BaseSocketRequest(in);
        }

        public BaseSocketRequest[] newArray(int size) {
            return new BaseSocketRequest[size];
        }
    };
}
