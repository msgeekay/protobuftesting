package com.msgeekay.btx.protobuftesting.websocks.service;

import android.app.NotificationManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.PowerManager;
import android.os.RemoteCallbackList;
import android.os.RemoteException;
import android.support.annotation.Nullable;
import android.util.Log;

import com.msgeekay.btx.protobuftesting.App;
import com.msgeekay.btx.protobuftesting.IRemoteServiceCallback;
import com.msgeekay.btx.protobuftesting.utils.Logs;
import com.msgeekay.btx.protobuftesting.utils.NotificationHelper;
import com.msgeekay.btx.protobuftesting.websocks.client.WebSocketClient;
import com.msgeekay.btx.protobuftesting.websocks.messages.request.BaseSocketRequest;
import com.msgeekay.btx.protobuftesting.websocks.messages.response.BaseSocketResponse;

import java.lang.ref.WeakReference;


import trading.Entity;

/**
 * Created by gkostyaev on 05/12/2017.
 */
public class WebSocketService extends Service implements CheckStateCallback {

    public static final String TAG = WebSocketService.class.getName();

    /**
     * This is a list of callbacks that have been registered with the
     * service.  Note that this is package scoped (instead of private) so
     * that it can be accessed more efficiently from inner classes.
     */
    final RemoteCallbackList<IRemoteServiceCallback> mCallbacks
            = new RemoteCallbackList<IRemoteServiceCallback>();

    private PowerManager.WakeLock wakeLock;

    private boolean isInitialized = false;
    private boolean isRunning;
    private boolean bound;

    private boolean isSocketing;

    private boolean mDebugLog = true;

    final Messenger mMessenger = new Messenger(new IncomingHandler(this));
    private NotificationManager mNotificationManager;
    private WebSocketServiceScheduler webSocketServiceScheduler;
    private WebSocketClient webSocketClient;

    public static final int MSG_START_SOCKET = 1;
    public static final int MSG_STOP_SOCKET = 2;
    public static final int MSG_CHECK_TO_STOP = 3;
    public static final int MSG_STOP_SERVICE = 4;

    public static final int MSG_DT_HELLO = 5;
    public static final int MSG_DT_2 = 6;
    public static final int MSG_DT_3 = 7;

    static class IncomingHandler extends Handler {

        WeakReference<WebSocketService> serviceWRef;

        public IncomingHandler(WebSocketService service)
        {
            this.serviceWRef = new WeakReference<WebSocketService>(service);
        }

        @Override
        public void handleMessage(Message msg) {

            if (serviceWRef.get() != null)
                Log.d(serviceWRef.get().getApplicationInfo().processName,
                    getClass().getName() + ", service handleMessage " + String.valueOf(msg.what));
            switch (msg.what) {
                case MSG_START_SOCKET:
                    if (serviceWRef.get() != null)
                        serviceWRef.get().startSocketing();
                    break;
                case MSG_STOP_SOCKET:
                    if (serviceWRef.get() != null)
                        serviceWRef.get().stopSocketing();
                    break;
                case MSG_CHECK_TO_STOP:
                    if (serviceWRef.get() != null)
                        serviceWRef.get().checkToStop();

                    break;
                case MSG_DT_HELLO:
                    //usecase to bd
                    if (serviceWRef.get() != null)
                        serviceWRef.get().sendHello();
                    break;
                case MSG_DT_2:
                    //usecase to bd
                    if (serviceWRef.get() != null)
                        serviceWRef.get().sendHello();
                    break;
                case MSG_DT_3:
                    //usecase to bd
                    if (serviceWRef.get() != null)
                        serviceWRef.get().sendHello();
                    break;
                default:
                    super.handleMessage(msg);
            }

            if (serviceWRef.get() != null)
                serviceWRef.get().checkServiceState();
        }
    }

    private WebSocketClient.SocketResponseListener  webSocketListener = new WebSocketClient.SocketResponseListener() {
        @Override
        public void onNewMessage(String message) {
            //handleMessage(new WebSocketEvent(WebSocketEvent.MESSAGE_EVENT, message));
            Log.e(TAG, "received TEXT from socket: " + message);
        }

        @Override
        public void onNewMessage(byte[] bytes) {

            //handleMessage(new WebSocketEvent(WebSocketEvent.BINARY_MESSAGE_EVENT, rawMessage));
            Log.e(TAG, "received BYTES from socket: " + bytes);

            try {
                trading.Entity.Event ev = trading.Entity.Event.parseFrom(bytes);
                Log.e(TAG, "event is parsed with type = " + ev.getType().name());
                Log.e(TAG, "event is parsed with data = " + ev.getData());
                if (ev.getType() == Entity.EventType.EventTypeTradeConfig) {
                    trading.Entity.TradeConfig tc = trading.Entity.TradeConfig.parseFrom(ev.getData());
                    Log.e(TAG, "TradeConfig is ok");

                    BaseSocketResponse response = new BaseSocketResponse(ev.getType().name(), ev.getData().toByteArray());
                    broadcastResponse(response);
                }
            }
            catch (Exception ex)
            {
                Log.e(TAG, "err: " + ex.toString());
            }
        }

        @Override
        public void onStatusChange(WebSocketClient.ConnectionStatus status) {
            if (status == WebSocketClient.ConnectionStatus.CONNECTED)
            {
                //EventBus.getDefault().post(new WebSocketIsOpenEvent());
                Log.e(TAG, "received STATUS from socket: " + "CONNECTED");
            }

            if (status == WebSocketClient.ConnectionStatus.DISCONNECTED
                    || status == WebSocketClient.ConnectionStatus.CONNECTERROR)
            {
                Log.e(TAG, "received STATUS from socket: " + "DISCONNECTED OR ERROR");
            }
        }
    };

    @Override
    public void onCreate() {
        super.onCreate();

        if (!isInitialized)
        {
            Log.d(this.getApplicationInfo().processName, getClass().getName() + " service onCreate initializing");

            PowerManager mgr = (PowerManager)getSystemService(Context.POWER_SERVICE);
            if (mgr != null) {
                wakeLock = mgr.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "BinatexServiceWakeLock");
                if (wakeLock != null)
                    wakeLock.acquire();
            }

            webSocketClient = new WebSocketClient(App.getInstance(), webSocketListener);
            webSocketServiceScheduler = new WebSocketServiceScheduler(this);
            mNotificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
            isInitialized = true;


        }

        isSocketing = App.getInstance().isSocketing();
        if (isSocketing)
            startSocketing();


        stopForeground(true);
    }

    private final IMessageBroker.Stub mBrokerBinder = new IMessageBroker.Stub() {

        @Override
        public void checkToStop() throws RemoteException {
            WebSocketService.this.checkToStop();
        }

        @Override
        public void stopService() throws RemoteException {
            stopService();
        }

        @Override
        public void startSocket() throws RemoteException {
            startSocketing();
        }

        @Override
        public void stopSocket() throws RemoteException {
            stopSocketing();
        }

        @Override
        public void sendRequest(BaseSocketRequest request) throws RemoteException {
            Logs.e(TAG, "request to send: " + request.getType().name());
            if (webSocketClient != null && request != null)
            {
                webSocketClient.sendMessage(request.getRequestAsBytes());
            }
        }

        @Override
        public void registerCallback(IRemoteServiceCallback cb) throws RemoteException {
            if (cb != null) mCallbacks.register(cb);
        }

        @Override
        public void unregisterCallback(IRemoteServiceCallback cb) throws RemoteException {
            if (cb != null) mCallbacks.unregister(cb);
        }
    };

    private void broadcastResponse(BaseSocketResponse response) {
        // Broadcast to all clients the new value.
        final int N = mCallbacks.beginBroadcast();
        for (int i=0; i<N; i++) {
            try {
                mCallbacks.getBroadcastItem(i).onServiceResponse(response);
            } catch (RemoteException e) {
                // The RemoteCallbackList will take care of removing
                // the dead object for us.
            }
        }
        mCallbacks.finishBroadcast();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        bound = true;

        IBinder retVal = mMessenger.getBinder();

        String action = intent.getAction();

        if (BaseAIDLServiceManager.SERVICE_AIDL_INTENT_ACTION.equals(action))
        {
            retVal = mBrokerBinder;
        }
        if (BaseMsgServiceManager.SERVICE_MSG_INTENT_ACTION.equals(action))
        {
            retVal = mMessenger.getBinder();
        }

        return retVal;
    }

    @Override
    public void onRebind(Intent intent) {
        super.onRebind(intent);
        bound = true;
    }

    @Override
    public boolean onUnbind(Intent intent) {
        stopSocketing();

        bound = false;
        return super.onUnbind(intent);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        stopSocketing();

        if (wakeLock != null && wakeLock.isHeld()){
            wakeLock.release();
        }

        Log.d(TAG,"service onDestroy");
        webSocketServiceScheduler.stop();
        // Unregister all callbacks.
        mCallbacks.kill();
        super.onDestroy();
        android.os.Process.killProcess(android.os.Process.myPid());
    }

    public void startSocketing() {
        if (webSocketClient == null)
            webSocketClient = new WebSocketClient(App.getInstance(), webSocketListener);

        if (!webSocketClient.isConnected())
            webSocketClient.connectToWebSocket();

        if (!this.isSocketing) {
            this.isSocketing = true;
            App.getInstance().setSocketing(true);
            logDebug("start socketing");
        }
    }


    public void stopSocketing() {
        if (webSocketClient != null && webSocketClient.isConnected()) {
            webSocketClient.disconnectWebSocket();
            App.getInstance().setSocketing(false);
            this.isSocketing = false;
            logDebug("stop socketing");
        }


    }


    public void checkServiceState() {
        if (!this.isRunning) {
            startService(new Intent(this, WebSocketService.class));
            setAsForeground();
            this.isRunning = true;
        }
    }

    private void setAsForeground() {
        if (NotificationHelper.SVC_FOREGROUND_ALLOWED_SETTING_ID) {
            logDebug("Foreground initialized");
            startForeground(NotificationHelper.NOTIFICATION_FOREGROUND, NotificationHelper.getDefaultNotification(this));
        }
    }

    public void stopService() {
        this.isRunning = false;
        stopForeground(true);
        stopSelf();
    }

    public void onCheckStateCallback() {
        checkToStop();
    }

    private void checkToStop() {
        if (!this.bound && canStop()) {
            App.getInstance().setSocketing(false);
            //Preferences.setBoolean(Preferences.IS_TRACKING, false, this);
            stopService();
        }
    }

    private boolean canStop() {

        return true;
    }

    private void logDebug(String msg) {
        if (this.mDebugLog) {
            Log.d(TAG, msg);
        }
    }


    /**
     * messages block
     * temporarilly inside service
     */

    public void sendHello()
    {
        trading.Entity.Hello hello = Entity.Hello.newBuilder().setUser(2).setAccount(1).build();
        trading.Entity.Event event = Entity.Event.newBuilder().setType(Entity.EventType.EventTypeHello)
                                        .setData(hello.toByteString()).build();

        webSocketClient.sendMessage(event.toByteArray());
    }
}
